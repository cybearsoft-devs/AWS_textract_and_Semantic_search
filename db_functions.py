import uuid
from uuid import UUID


def write_into_documents_collection(vector: [], text: str, index, id: str, extension):
    upsert_response = index.upsert(
        vectors=[
            (id, vector, {"text": text, "extension": f"{extension}"}),
        ],
        namespace="whole documents"
    )

    return True


def search_simular_documents(vector, top_k, index):
    results = index.query(
        vector=vector,
        top_k=top_k,
        include_values=True,
        namespace='whole documents',
        includeMetadata=True,
        includeValues=False,
    )

    return results


def get_document_by_id(index, id):
    results = index.query(
        id=id,
        includeMetadata=True,
        includeValues=False,
        namespace='whole documents',
        top_k=1,
    )
    return results.to_dict()


def extract_text_from_one_index_document(document):
    return document['matches'][0]['metadata']['text']
