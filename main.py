import json
from fastapi import FastAPI, HTTPException

from aws_textract import upload_to_s3_and_extract_text
from serializers import *
from db_functions import *
from open_ai import *
from config import *
import boto3
from fastapi import FastAPI, UploadFile, File, HTTPException
import boto3
import uuid
import time
import asyncio

load_dotenv()

app = FastAPI()


def db_global_initialization():
    import pinecone
    global index, pinecone, s3, textract_client
    pinecone.init(api_key=PINECONE_API_KEY, environment=PINECONE_ENV)
    index = pinecone.Index("second")

    session = boto3.Session(
        aws_access_key_id=ACCESS_KEY,
        aws_secret_access_key=ACCESS_SECRET_KEY,
        region_name=REGION
    )

    s3 = session.client('s3')
    textract_client = session.client('textract')


db_global_initialization()


@app.post('/db_upsert')
def write_doc_into_db(upsert_data: UpsertRequest):
    if upsert_data:
        vector = vectorize(upsert_data.text)
        write_into_documents_collection(vector, upsert_data.text, index, upsert_data.id, upsert_data.extension)
        return True


@app.post('/semantic_search')
async def semantic_search(data: SemanticSearchRequest):
    vector = vectorize(data.text)
    response = search_simular_documents(vector, data.max_mount_of_required_results, index)
    if response.matches:
        return json.dumps(response.to_dict())
    else:
        return HTTPException(status_code=404, detail="Matches didn't found")


@app.post("/upload_document/")
async def upload_and_extract_text(file: UploadFile = File(...)):
    if file.content_type not in ["application/pdf", "image/jpg", "image/jpeg", "image/png"]:
        raise HTTPException(
            status_code=400,
            detail=f"File type of {file.content_type} is not supported",
        )
    return await upload_to_s3_and_extract_text(file, textract_client, s3)


@app.post('/ask_a_question_to_the_document')
async def ask_a_question_to_the_document(question_data: AskQuestionToTheDocument):
    document_text = get_answer_from_document(question_data.document_id, question_data.question, index)
    document_text = unidecode(document_text)
    return json.dumps(unidecode(document_text))
