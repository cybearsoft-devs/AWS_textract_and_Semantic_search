import os

from dotenv import load_dotenv

load_dotenv()

OPEN_AI_TOKEN = os.getenv("OPEN_AI_TOKEN")
ADDRESS_OF_OPEN_AI_EMBEDDINGS = 'https://api.openai.com/v1/embeddings'
HEADERS = {"Authorization": f"Bearer {OPEN_AI_TOKEN}",
           "Content-Type": "application/json"}

PINECONE_API_KEY = os.getenv('PINECONE_API_KEY')
PINECONE_ENV = os.getenv('PINECONE_ENV')
ACCESS_KEY = os.getenv('ACCESS_KEY')
ACCESS_SECRET_KEY = os.getenv('ACCESS_SECRET_KEY')
REGION = os.getenv('REGION')
BUCKET = os.getenv('BUCKET')
