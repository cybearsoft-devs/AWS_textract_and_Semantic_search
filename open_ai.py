import json

import openai
import requests
from config import ADDRESS_OF_OPEN_AI_EMBEDDINGS, HEADERS, OPEN_AI_TOKEN
from db_functions import get_document_by_id, extract_text_from_one_index_document
from langdetect import detect  # A language detection library
from unidecode import unidecode
import time


def vectorize(text: str) -> []:
    data = {"input": text,
            "model": "text-embedding-ada-002"}
    vector = requests.post(url=ADDRESS_OF_OPEN_AI_EMBEDDINGS, headers=HEADERS, json=data)
    vector = vector.json().get('data')[0]['embedding']
    return vector


def get_answer_from_document(document_id, question, index):
    document_text = extract_text_from_one_index_document(get_document_by_id(index, document_id))
    document_language = detect(document_text)
    response = openai.ChatCompletion.create(
        api_key=OPEN_AI_TOKEN,
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": f"Translate the following {document_language} text:"},
            {"role": "user", "content": f"Document: {document_text}"},
            {"role": "user", "content": f"Question: {question}"},
            {"role": "system", "content": f"Answer: "}
        ]
    )
    return response["choices"][0]["message"]["content"]
