from fastapi import FastAPI, UploadFile, File, HTTPException
import boto3
import uuid
import time
import asyncio
from config import BUCKET


async def extractor_waiter(job_id, textract_client):
    while True:
        response = textract_client.get_document_text_detection(JobId=job_id)
        status = response['JobStatus']
        if status == 'SUCCEEDED':
            return response
        elif status == 'FAILED':
            raise Exception("Textract job failed")
        await asyncio.sleep(1)


async def upload_to_s3_and_extract_text(file, textract_client, s3):
    file_id = str(uuid.uuid4())
    extension = file.content_type.split("/")[-1]
    filename = file_id + "." + extension
    s3.upload_fileobj(file.file, BUCKET, filename)

    response = textract_client.start_document_text_detection(
        DocumentLocation={
            'S3Object': {
                'Bucket': BUCKET,
                'Name': filename
            }
        }
    )

    job_id = response['JobId']
    result_response = await extractor_waiter(job_id, textract_client)
    extracted_text = ""
    for item in result_response['Blocks']:
        if item['BlockType'] == 'LINE':
            extracted_text += item['Text'] + " "

    return {"id": file_id, "extension": extension, "extracted_text": extracted_text}
