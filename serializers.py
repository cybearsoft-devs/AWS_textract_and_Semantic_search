from pydantic import BaseModel, validator
from uuid import UUID


class UpsertRequest(BaseModel):
    text: str
    id: str
    extension: str


class SemanticSearchRequest(BaseModel):
    text: str
    max_mount_of_required_results: int


class SemanticSearchResponse(BaseModel):
    text: str


class AskQuestionToTheDocument(BaseModel):
    question: str
    document_id: str
